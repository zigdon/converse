from django.conf.urls.defaults import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('registration.urls')),
)

# For this test site, we just plug all of converse's URLs in at the top level.
urlpatterns += patterns('',
    url(r'^', include('converse.urls')),
)
