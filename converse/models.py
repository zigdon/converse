from collections import defaultdict
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.urlresolvers import reverse
from django.utils.html import escape
from django.template.defaultfilters import slugify
from converse.settings import DEFAULT_MODERATION

class Topic(models.Model):
    """A topic is the equivalent of a typical forum thread - a general topic that people want to discuss."""

    title = models.CharField(max_length=100)
    summary = models.TextField()

    creator = models.ForeignKey(User)
    editor = models.ForeignKey(User, blank=True, null=True, related_name='+')

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    is_active = models.BooleanField(default=True)
    is_locked = models.BooleanField(default=False)
    is_held = models.BooleanField(default=DEFAULT_MODERATION)
    needs_moderation = models.BooleanField(default=DEFAULT_MODERATION)

    def __unicode__(self):
        return self.title

    @models.permalink
    def permalink(self):
        return ('converse.views.topic', [self.id, slugify(self.title)])

    def has_new(self, user):
        try:
            seen = LatestSeen.objects.get(user=user, topic=self)
        except LatestSeen.DoesNotExist:
            return True
        if hasattr(self, 'last_update'):
            return self.last_update > seen.latest
        return seen.has_new()

    def subtopic_tree(self):
        tree = defaultdict(list)
        for subtopic in self.subtopic_set.all().select_related(depth=1):
            parent_items = tree[subtopic.parent.id if subtopic.parent else None]
            parent_items.append(subtopic)
            parent_items.append(tree[subtopic.id])
        return tree[None]

    def subtopic_rows(self):
        rows = []
        def add_recursive(node, depth=0):
            if isinstance(node, list):
                for item in node:
                    add_recursive(item, depth+1)
            else:
                rows.append({
                    'id': node.id,
                    'summary': node.summary,
                    'depth': depth*20,
                    'permalink': node.permalink,
                })
        add_recursive(self.subtopic_tree())
        return rows

    def subtopics_new(self, user):
        subtopics = self.subtopic_set.all().annotate(last_update=models.Max('edit__timestamp'))
        return set(s.id for s in subtopics if s.has_new(user))

    @classmethod
    def all_with_last_update(cls):
        return cls.objects.annotate(last_update=models.Max('edit__timestamp'))\
                     .select_related('creator')\
                     .filter(is_active=True,is_held=False)


class Subtopic(models.Model):
    """A subtopic is a specific aspect of a topic (potentially with its own subtopics).

    Subtopics have:
        * a bullet point in the conversation outline
        * their own linear thread of comments
        * zero or more subtopic children for a breakdown of the matter being discussed
    """

    summary = models.CharField(max_length=200)
    topic = models.ForeignKey(Topic)
    parent = models.ForeignKey('self', null=True)

    creator = models.ForeignKey(User)
    editor = models.ForeignKey(User, blank=True, null=True, related_name='+')

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.summary

    def permalink(self):
        return '%s#subtopic-%d' % (self.topic.permalink(), self.id)

    def has_new(self, user):
        try:
            seen = LatestSeen.objects.get(user=user, subtopic=self)
        except LatestSeen.DoesNotExist:
            return True
        if hasattr(self, 'last_update'):
            return self.last_update > seen.latest
        return seen.has_new()

    @classmethod
    def all_with_last_update(cls):
        return cls.objects.annotate(last_update=models.Max('edit__timestamp'))

    def all_visible_comments(self, user):
        comments = self.comment_set
        if not user.is_authenticated():
            comments = comments.filter(is_held=False, is_active=True)
        elif not user.has_perm('converse.can_moderate'):
            comments = comments.filter(models.Q(is_held=False, is_active=True) | models.Q(creator=user))
        return comments

class Comment(models.Model):
    """A comment is a more conversational text entry associated with a particular subtopic."""

    content = models.TextField()
    subtopic = models.ForeignKey(Subtopic)

    is_system = models.BooleanField(editable=False, default=False)

    creator = models.ForeignKey(User)
    editor = models.ForeignKey(User, blank=True, null=True, related_name='+')

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    is_active = models.BooleanField(default=True)
    is_held = models.BooleanField(default=False)

    def __unicode__(self):
        return self.content

    def permalink(self):
        return '%s#subtopic-%d,comment-%d' % (self.subtopic.topic.permalink(), self.subtopic.id, self.id)

    @classmethod
    def all_with_last_update(cls):
        return cls.objects.filter(is_active=True, is_held=False).annotate(last_update=models.Max('edit__timestamp'))


class Edit(models.Model):
    """An edit is a modification to something in a topic (topic itself, subtopic, or comment)."""

    # This is always set (for easy aggregation)
    topic = models.ForeignKey(Topic, editable=False)

    # These won't always be set:
    #  * subtopic won't be set for topic edits, only subtopic+comment
    #  * comment won't be set for topic or subtopic edits, only comment
    subtopic = models.ForeignKey(Subtopic, null=True, editable=False)
    comment = models.ForeignKey(Comment, null=True, editable=False)

    user = models.ForeignKey(User, editable=False)
    timestamp = models.DateTimeField(auto_now_add=True)

# Signal receivers to automate edit logging
@receiver(post_save, sender=Topic)
def topic_post_save(sender, instance, **kwargs):
    edit = Edit(
        topic=instance,
        user=instance.editor or instance.creator,
    )
    edit.save()

@receiver(post_save, sender=Subtopic)
def subtopic_post_save(sender, instance, created, **kwargs):
    edit = Edit(
        topic=instance.topic,
        subtopic=instance,
        user=instance.editor or instance.creator,
    )
    edit.save()
    if created and instance.parent:
        forward_text = '''Added child subtopic <a href="%s">%s</a>.'''
        backward_text = '''Added from <a href="%s">here</a>.'''
        forward_comment = Comment(
            content=forward_text % (instance.permalink(), escape(instance.summary)),
            subtopic=instance.parent,
            creator=instance.creator,
            is_system=True,
        )
        forward_comment.save()
        backward_comment = Comment(
            content=backward_text % forward_comment.permalink(),
            subtopic=instance,
            creator=instance.creator,
            is_system=True,
        )
        backward_comment.save()

@receiver(post_save, sender=Comment)
def comment_post_save(sender, instance, **kwargs):
    edit = Edit(
        topic=instance.subtopic.topic,
        subtopic=instance.subtopic,
        comment=instance,
        user=instance.editor or instance.creator,
    )
    edit.save()

class LatestSeen(models.Model):
    topic = models.ForeignKey(Topic)
    subtopic = models.ForeignKey(Subtopic, null=True)
    user = models.ForeignKey(User)
    latest = models.DateTimeField(auto_now=True)

    @classmethod
    def mark_seen(cls, user, topic=None, subtopic=None):
        if topic is None and subtopic is None:
            raise ValueError("Either 'topic' or 'subtopic' parameter must be specified for mark_seen")
        if topic and subtopic:
            raise ValueError("mark_seen expects only one of 'topic' or 'subtopic', not both")

        try:
            if topic:
                lastseen = cls.objects.get(user=user, topic=topic, subtopic=None)
            else:
                lastseen = cls.objects.get(user=user, subtopic=subtopic)
        except cls.DoesNotExist:
            if topic:
                lastseen = cls(user=user, topic=topic, subtopic=None)
            else:
                lastseen = cls(user=user, topic=subtopic.topic, subtopic=subtopic)

        lastseen.save()

    def has_new(self):
        if self.subtopic:
            editlog = Edit.objects.filter(user=self.user, subtopic=self.subtopic)
        else:
            editlog = Edit.objects.filter(user=self.user, topic=self.topic)

        result = editlog.aggregate(ts=models.Max('timestamp'))
        if result['ts'] is None:
            return False
        if result['ts'] <= self.latest:
            return False
        return True
