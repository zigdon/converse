from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from converse.models import Topic, Subtopic, Comment
from converse.management.commands.base import ConverseCommand
import random

class Command(ConverseCommand):
    help = "Edit a number of subtopics and post a few new comments"

    def handle(self, *args, **options):
        # edit subtopics
        for s in random.sample(self.all_subtopics, 20):
            s.editor = self._user()
            s.save()

        # create another topic
        t = Topic.objects.create(title=self._words(15),
                                 summary=self._words(50, newlines=True),
                                 creator=self._user())

        t.created = self._date()

        # create more subtopics
        for t in random.sample(self.all_topics, 20):
            if random.random() < 0.2:
                parent = None
            else:
                peers = Subtopic.objects.filter(topic=t)
                if peers:
                    parent = random.sample(peers, 1)[0]
                else:
                    parent = None


            s = Subtopic.objects.create(summary=self._words(20),
                                        topic=t,
                                        parent=parent,
                                        creator=self._user())

        # create more comments
        for s in random.sample(self.all_subtopics, 100):
            c = Comment.objects.create(content=self._words(50, newlines=True),
                                       subtopic=s,
                                       creator=self._user())
            c.save()

        # edit topics
        for t in random.sample(self.all_topics, 5):
            t.title=self._words(10)
            t.summary=self._words(40, newlines=True)
            t.save()

        # edit subtopics
        for s in random.sample(self.all_subtopics, 10):
            s.summary=self._words(20)
            s.save()

        # edit comments
        for c in random.sample(self.all_comments, 10):
            c.content=self._words(50, newlines=True)
            c.save()
