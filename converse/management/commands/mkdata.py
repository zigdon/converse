from django.contrib.auth.models import User
from converse.models import Topic, Subtopic, Comment
from converse.management.commands.base import ConverseCommand
import random

class Command(ConverseCommand):
    help = "Generate fake users, topics, subtopics and comments"

    def handle(self, *args, **options):
        # create users
        print "Creating users..."
        while User.objects.all().count() < 20:
            username = self._username()
            User.objects.create_user(username, username+"@example.com", "test")

        self.all_users = User.objects.all()

        # create topics
        print "Creating topics..."
        while Topic.objects.all().count() < 100:
            t = Topic.objects.create(title=self._words(15),
                                     summary=self._words(50, newlines=True),
                                     creator=self._user())

            t.created = self._date()
            if random.random() < 0.5:
                t.modified = self._date(start=t.created)
                t.editor = self._user()
            t.save()
        self.all_topics = Topic.objects.all()

        # create subtopics
        print "Creating subtopics..."
        while Subtopic.objects.all().count() < 200:
            t = random.sample(self.all_topics, 1)[0]
            if random.random() < 0.2:
                parent = None
            else:
                peers = Subtopic.objects.filter(topic=t)
                if peers:
                    parent = random.sample(peers, 1)[0]
                else:
                    parent = None


            s = Subtopic.objects.create(summary=self._words(20),
                                        topic=t,
                                        parent=parent,
                                        creator=self._user())
            if random.random() < 0.1:
                s.modified = self._date(start=s.created)
                s.editor = self._user()
            s.save()
        self.all_subtopics = Subtopic.objects.all()

        # create comments
        print "Creating comments..."
        while Comment.objects.all().count() < 1000:
            parent = random.sample(self.all_subtopics, 1)[0]

            c = Comment.objects.create(content=self._words(200, newlines=True),
                                       subtopic=parent,
                                       creator=self._user())
            if random.random() < 0.1:
                c.modified = self._date(start=c.created)
                c.editor = self._user()
            c.save()
